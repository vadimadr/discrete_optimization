
#ifndef DISCRETE_OPTIMIZATION_IMCP_SOLVER_HPP
#define DISCRETE_OPTIMIZATION_IMCP_SOLVER_HPP

#include <chrono>
#include "graph.hpp"

class MaxCliqueResult {
public:
    enum status_e {
        UNKNOWN,
        EXACT,
        HEURISTIC,
        TIME_LIMIT
    };

    status_e status;
    int lower_bound, upper_bound;
    std::vector<int> nodes;
};

class IMCPSolver {
protected:

    using clock = std::chrono::steady_clock;
    using time_point = clock::time_point;
    using ms_fp  = std::chrono::duration<float, std::chrono::milliseconds::period>;

    time_point start_, end_;
    ms_fp time_limit_;
    bool time_limited = false;
    bool check_time();

public:
    size_t lower_, upper_;
    void set_time_limit(ms_fp);
    double get_secs();

    virtual MaxCliqueResult solve(Graph &) = 0;
};



#endif //DISCRETE_OPTIMIZATION_IMCP_SOLVER_HPP
