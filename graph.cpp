
#include <sstream>
#include <fstream>
#include "graph.hpp"
#include "utils.hpp"

#include <algorithm>

Graph GraphReader::read_graph(const std::string &filename) {
    std::ifstream input_file(filename);

    massert(!input_file.fail(), "Could not open file " + filename);

    Graph graph{};
    int nodes = 0, edges = 0, edges_read = 0;

    std::string line;
    while (std::getline(input_file, line)) {
        std::stringstream ss(line);
        char c;
        int from_, to_;
        std::string format;

        ss >> c;

        switch (c) {
            case 'c':
                continue;
            case 'p':
                massert(nodes == 0 && edges == 0, "format token should occur only once");
                ss >> format >> nodes >> edges;
                graph.nodes.resize(static_cast<unsigned long>(nodes));
                for (size_t i = 0; i < nodes; ++i) {
                    graph.nodes[i].origign = i;
                }
                massert(format == "edge", "Only edge format is supported");
                break;
            case 'e':
                ss >> from_ >> to_;
                add_edge(from_ - 1, to_ - 1, graph);
                edges_read++;
                break;
            default:
                massert(false, "Unknown token - " + std::to_string(c));
        }
    }
    massert(nodes > 0, "Wrong graph, 0 vertices found");
    massert(edges_read == edges, "Declared number of edges does not match actual");

    int n_duplicates = 0;

    for (auto &node: graph.nodes) {
        std::sort(node.adj.begin(), node.adj.end());
        auto it = std::unique(node.adj.begin(), node.adj.end());
        n_duplicates += std::distance(it, node.adj.end());
        node.adj.erase(it, node.adj.end());
    }

    massert((n_duplicates == 0) || n_duplicates == edges, "Graph should be undirected");

    if (n_duplicates == edges) {
        edges /= 2;
    }


    double max_w = nodes * (nodes - 1) / 2.;
    double w = edges / max_w;

    std::cout << "Read graph with " << nodes << " vertices, " << edges_read << " edges, w = " << w << std::endl;
    return graph;
}

void GraphReader::add_edge(int from_, int to_, Graph &graph) {
    graph.nodes[from_].adj.push_back(to_);
    graph.nodes[to_].adj.push_back(from_);
}

bool Graph::is_adjacent(int from_, int to_) {
    massert(from_ < nodes.size() && to_ < nodes.size(), "Unknown node");

    std::vector<int> &adj = nodes[from_].adj;
    return std::find(adj.begin(), adj.end(), to_) != adj.end();
}

bool Graph::is_clique(const std::vector<int> &elements) {
    for (int u: elements) {
        for (int v: elements) {
            if (u == v)
                continue;
            if (!is_adjacent(u, v))
                return false;
        }
    }
    return true;
}

