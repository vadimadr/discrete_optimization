
#ifndef DISCRETE_OPTIMIZATION_MCPKCOREHEURISTIC_HPP
#define DISCRETE_OPTIMIZATION_MCPKCOREHEURISTIC_HPP

#include "imcp_solver.hpp"
class MCPKCoreHeuristic : public IMCPSolver {
    void get_k_cores(Graph &graph);
    size_t get_colors(Graph &graph);

    Graph *graph_;
public:

    MaxCliqueResult solve(Graph &graph) override;
    std::vector<int> k_core, k_core_perm;

    int max_core;
    void branch(std::vector<int> &p_vec, int sz, int &mc_current, std::vector<int> &c_vec, std::vector<bool> &ind);
    std::vector<int> degree;
    int md;
};

#endif //DISCRETE_OPTIMIZATION_MCPKCOREHEURISTIC_HPP
