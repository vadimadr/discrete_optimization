
#include "imcp_solver.hpp"

bool IMCPSolver::check_time() {
    return !time_limited || (clock::now() - start_) < time_limit_;
}

double IMCPSolver::get_secs() {
    auto duration = end_ - start_;
    double duration_ms = std::chrono::duration_cast<ms_fp>(duration).count();
    return duration_ms / 1000.0;
}

void IMCPSolver::set_time_limit(IMCPSolver::ms_fp limit_) {
    time_limit_ = limit_;
    time_limited = true;
}
