
#ifndef DISCRETE_OPTIMIZATION_GRAPH_HPP
#define DISCRETE_OPTIMIZATION_GRAPH_HPP

#include <vector>
#include <string>

class GraphNode {
public:
    using adj_type = std::vector<int>;
    /* Adjacency list */
    adj_type adj;

    int origign;
};

class Graph {
public:
    using adj_type = GraphNode::adj_type;
    std::vector<GraphNode> nodes;

    bool is_adjacent(int, int);
    bool is_clique(const std::vector<int> &elements);
};

class GraphReader {
    static void add_edge(int from_, int to_, Graph &);
public:
    static Graph read_graph(const std::string&);
};

#endif //DISCRETE_OPTIMIZATION_GRAPH_HPP
