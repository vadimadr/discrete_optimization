
#include <iostream>
#include <algorithm>
#include <set>
#include <unordered_map>
#include "mcp_brute_force.hpp"

MaxCliqueResult MCPBruteForceSolver::solve(Graph &graph) {

    initialize_mapping(graph);
    start_ = clock::now();

    MaxCliqueResult best_clique{};
    lower_ = 1, upper_ = graph.nodes.size() + 1;

    size_t num_nodes = graph.nodes.size();
    bool improved = true;


    for (size_t k = lower_; k < upper_; ++k) {
        num_nodes -= reduce_graph(graph, k - 1);

        bool found = false;
        if (!improved) break;
        improved = false;

        size_t n_jobs = 1;
        std::vector<subset_t> p_vec(n_jobs);

        int64_t max_j = num_subgraphs(k, num_nodes);
        int64_t iters_per_job = max_j / n_jobs;

        for (size_t j = 0; j < n_jobs; ++j) {
            kth_subset(j * iters_per_job, k, num_nodes, p_vec[j], 0);
        }

//        #pragma omp parallel for
        for (size_t j = 0; j < max_j; ++j) {
            if (found || num_nodes == 0)
                break;

            size_t p_ind = j / iters_per_job;

            if (p_ind == 0 && j % 1000 == 0 && !check_time()) {
                found = true;
                break;
            }

            if (graph.is_clique(p_vec[p_ind])) {
                found = true;
                improved = true;
                best_clique.nodes = p_vec[p_ind];
                remap_nodes(best_clique.nodes, mapping);
                break;
            }

            next_subset(p_vec[p_ind], num_nodes);
        }
    }

    end_ = clock::now();
    return best_clique;

}


size_t MCPBruteForceSolver::reduce_graph(Graph &graph, int64_t k) {
    std::set<int> mark;
    for (size_t i = 0; i < graph.nodes.size(); ++i) {
        if (graph.nodes[i].adj.size() < k) {
            mark.insert(i);
        }
    }

    if (mark.empty()) {
        return 0;
    }

    std::cout << "k = " << mark.size() << " pruned\n";

    std::unordered_map<int, int> d;
    std::vector<int> m(graph.nodes.size()), new_mapping;
    for (size_t i = 0, j = 0; i < graph.nodes.size(); ++i) {
        if (mark.count(i) != 0)
            continue;
        new_mapping.push_back(mapping[i]);
        m[i] = j++;
    }

    std::swap(mapping, new_mapping);

    int l = 0;
    for (auto it = graph.nodes.begin(); it != graph.nodes.end(); ++l) {

        // remove marked node
        if (mark.count(l) > 0) {
            it = graph.nodes.erase(it);
            continue;
        }

        // remove marked nodes from adjacency lists
        std::vector<int> &adj = it->adj;
        std::vector<int> new_adj(adj.size());
        auto it2 = std::set_difference(adj.begin(), adj.end(), mark.begin(), mark.end(), new_adj.begin());
        new_adj.resize(std::distance(new_adj.begin(), it2));

        // reset indicies for nodes
        remap_nodes(new_adj, m);

        std::swap(it->adj, new_adj);
        ++it;
    }

    return mark.size();
}

int64_t MCPBruteForceSolver::num_subgraphs(int64_t k, int64_t n) {
    int64_t res = 1;
    for (size_t i = 1; i < k + 1; ++i) {
        res = res * (n - i + 1) / i;
    }
    return res;
}

void MCPBruteForceSolver::initialize_mapping(Graph &graph) {
    mapping.resize(graph.nodes.size());
    for (size_t i = 0; i < graph.nodes.size(); ++i) {
        mapping[i] = graph.nodes[i].origign;
    }
}

bool MCPBruteForceSolver::next_subset(subset_t &p, size_t n) {
    auto k = static_cast<int>(p.size());
    for (int i = k - 1; i >= 0; i--) {
        if (p[i] < n - k + i) {
            p[i]++;
            for (int j = 1; j < k - i; ++j)
                p[i + j] = p[i] + j;
            return true;
        }
    }
    return false;
}

void MCPBruteForceSolver::kth_subset(int64_t k, int r, size_t n, subset_t &res, int l) {
    if (r == 0 || n == 0) return;

    if (n == l + r) {
        for (int i = 0; i < n - l; ++i) res.push_back(l + i);
        return;
    }

    int64_t i = num_subgraphs(n - l - 1, r - 1);
    if (k < i) {
        res.push_back(l);
        return kth_subset(k, r - 1, n, res, l + 1);
    }

    return kth_subset(k - i, r, n, res, l + 1);
}

void MCPBruteForceSolver::remap_nodes(Graph::adj_type &result, std::vector<int> const &mapping) {
    for (auto &node: result) {
        node = mapping[node];
    }
}

