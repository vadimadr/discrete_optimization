//
// Created by vadim on 12/5/17.
//

#ifndef DISCRETE_OPTIMIZATION_MCPIPSOLVER_H
#define DISCRETE_OPTIMIZATION_MCPIPSOLVER_H

#include <ilcplex/ilocplex.h>
#include <unordered_set>
#include <map>
#include "imcp_solver.hpp"
ILOSTLBEGIN

class Bound {
  IloRange r;
  bool default_;

 public:
  Bound(IloRange r) : r(r), default_(false) {}
  Bound() : default_(true) {}
  ~Bound() { r.end(); }
  operator bool() const { return !default_; }
  const IloRange& get() const { return r; }

  bool operator==(const Bound &);
};

namespace std {
template<>
struct hash<Bound> {
  size_t operator()(const Bound &b) const {
    std::map<int, float > m1;

    IloExpr::LinearIterator it;
    for (it = IloExpr(b.get().getExpr()).getLinearIterator(); it.ok(); ++it) {
      m1.emplace(it.getVar().getId(), it.getCoef());
    }
    m1.emplace(-1, b.get().getLb());
    m1.emplace(-2, b.get().getUb());

    size_t h = m1.size();
    for (auto &item: m1) {
      size_t h_ = std::hash<float>()(item.second);
      h ^= h_ ^ item.first + 0x9e3779b9 + (h_ << 2) + (h_ >> 6);
    }

    return h;
  }
};
}

// integer programming solver
class McpIpSolver : public IMCPSolver {
  IloEnv   env;
  IloNumVarArray var;
  IloRangeArray con;
  IloModel model;
  IloObjective obj;
  IloCplex cplex;

  Graph *graph;

  MaxCliqueResult best;
  std::vector<IloRange> bounds;
  std::unordered_set<Bound> heu_bounds;
  std::unordered_set<size_t> branch_vars;
  size_t lb, ub;

  std::mt19937 rd;

 public:
  McpIpSolver();

  MaxCliqueResult solve(Graph &graph) override;

  void define_ip_model(Graph &graph);
  MaxCliqueResult extract_clique();
  void _print_solution() const;
  void _print_model();
  int branch(const Bound &bound = Bound{});
  pair<vector<int>, int> get_coloring();
  double try_branch(IloRange range);
  std::vector<int> get_independent_set();
};

#endif //DISCRETE_OPTIMIZATION_MCPIPSOLVER_H
