
#include <algorithm>
#include <unordered_set>
#include "mcp_kcore_heu.hpp"

using std::vector;
MaxCliqueResult MCPKCoreHeuristic::solve(Graph &graph) {
    get_k_cores(graph);

    graph_ = &graph;

    MaxCliqueResult res{};

    upper_ = max_core + 1;

    degree.resize(graph.nodes.size());
    md = 0;
    for (size_t l = 0; l < degree.size(); ++l) {
        degree[l] = graph.nodes[l].adj.size();
        md = std::max(md, degree[l]);
    }

    vector<int> C, p_vec;
    C.reserve(upper_);
    res.nodes.reserve(upper_);
    p_vec.reserve(md + 1);
    vector<bool > ind(graph.nodes.size(), 0);

    bool found = false;
    int max_ = 0, mc_prev, mc_cur, i, v;

    for (i = graph.nodes.size() - 1; i >= 0; --i) {
        if (found)
            continue;

        v = k_core_perm[i];
        mc_prev = mc_cur = max_;

        if (k_core[v] > max_) {
            for (int edge : graph.nodes[v].adj) {
                if (k_core[edge] > max_) {
                    p_vec.push_back(edge);
                }
            }

            if (p_vec.size() > mc_cur) {
                std::sort(p_vec.begin(), p_vec.end(), [&](int v1, int v2) {
                    return k_core[v1] < k_core[v2];
                });

                branch(p_vec, 1, mc_cur, C, ind);

                if (mc_cur > mc_prev) {
                    if (max_ < mc_cur) {
                        if (max_ < mc_cur) {
                            max_ = mc_cur;
                            C.push_back(v);
                            res.nodes = C;
                            if (max_ >= upper_)
                                found = true;
                        }
                    }
                }
            }
            C.clear();
            p_vec.clear();
        }
    }

    upper_ = std::min(upper_, get_colors(graph));

    return res;
}

void MCPKCoreHeuristic::branch(vector<int> &p_vec,
                               int sz,
                               int &mc_current,
                               vector<int> &c_vec,
                               vector<bool> &ind) {
    if (p_vec.size() == 0) {
        if (sz > mc_current) {
            mc_current = sz;
        }
        return;
    }

    int u = p_vec.back();
    p_vec.pop_back();

    for (auto &item: graph_->nodes[u].adj) {
        ind[item] = true;
    }

    std::vector<int> intersect;
    intersect.reserve(p_vec.size());
    for (int i : p_vec)
        if (ind[i] && k_core[i] > mc_current)
            intersect.push_back(i);

    for (auto &item: graph_->nodes[u].adj) {
        ind[item] = false;
    }

    int mc_prev = mc_current;
    branch(intersect, sz + 1, mc_current, c_vec, ind);

    if (mc_current > mc_prev)  c_vec.push_back(u);

    intersect.clear();
    p_vec.clear();
}

void MCPKCoreHeuristic::get_k_cores(Graph &graph) {
    /*
     * An O(m) Algorithm for Cores Decomposition of Networks Vladimir Batagelj and Matjaz Zaversnik,
     * 2003. http://arxiv.org/abs/cs.DS/0310049
    */

    long long j;
    int n, d, i, total, tmp, md;
    int v, u, w, du, pu, pw, md_end;
    n = graph.nodes.size() + 1;

    k_core_perm.clear();
    k_core.clear();

    vector<int> pos(n);
    k_core.resize(n, 0);
    k_core_perm.resize(n, 0);

    md = 0;
    for (v = 1; v < n; v++) {
        k_core[v] = graph.nodes[v - 1].adj.size();
        if (k_core[v] > md)
            md = k_core[v];
        md = std::max(md, k_core[v]);
    }

    md_end = md + 1;
    vector<int> bin(md_end, 0);

    for (v = 1; v < n; v++)
        bin[k_core[v]]++;

    total = 1;
    for (d = 0; d < md_end; d++) {
        tmp = bin[d];
        bin[d] = total;
        total = total + tmp;
    }

    for (v = 1; v < n; v++) {
        pos[v] = bin[k_core[v]];
        k_core_perm[pos[v]] = v;
        bin[k_core[v]]++;
    }

    for (d = md; d > 1; d--)
        bin[d] = bin[d - 1];
    bin[0] = 1;

    for (i = 1; i < n; i++) {
        v = k_core_perm[i];
        for (j = 0; j < graph.nodes[v - 1].adj.size(); j++) {
            u = graph.nodes[v - 1].adj[j] + 1;
            if (k_core[u] > k_core[v]) {
                du = k_core[u];
                pu = pos[u];
                pw = bin[du];
                w = k_core_perm[pw];
                if (u != w) {
                    pos[u] = pw;
                    k_core_perm[pu] = w;
                    pos[w] = pu;
                    k_core_perm[pw] = u;
                }
                bin[du]++;
                k_core[u]--;
            }
        }
    }

    for (v = 0; v < n - 1; v++) {
        k_core[v] = k_core[v + 1] + 1;
        k_core_perm[v] = k_core_perm[v + 1] - 1;
    }
    max_core = k_core[k_core_perm[graph.nodes.size() - 1]] - 1;
}
size_t MCPKCoreHeuristic::get_colors(Graph &graph) {
    // greedy coloring upper bound
    std::vector<int> colors(graph.nodes.size(), -1);
    colors[0] = 0;

    int num_colors = 0;
    for (size_t i = 0; i < graph.nodes.size(); ++i) {
        std::unordered_set<int> used_colors;
        for (auto &neighbour: graph.nodes[i].adj) {
            used_colors.insert(colors[neighbour]);
        }

        bool colored = false;
        for (int c = 0; c < num_colors; ++c) {
            if (used_colors.find(c) == used_colors.end()) {
                colored = true;
                colors[i] = c;
                break;
            }
        }

        if (!colored) {
            colors[i] = num_colors++;
        }
    }

    return num_colors;

}

