//
// Created by vadim on 12/5/17.
//

#include <unordered_map>
#include <numeric>
#include <algorithm>
#include "mcp_ip_solver.h"
#include "mcp_kcore_heu.hpp"

template <typename T>
bool is_integer(T val, T eps = 1e-6) {
    return fabs(val - floor(val)) < eps;
}

McpIpSolver::McpIpSolver() : var(env), con(env), model(env) {}

MaxCliqueResult McpIpSolver::solve(Graph &graph_) {
  MCPKCoreHeuristic k_core_heu;
  start_ = clock::now();
  const MaxCliqueResult &heuristic_res = k_core_heu.solve(graph_);
  upper_ = k_core_heu.upper_;
  best = heuristic_res;
  lower_ = heuristic_res.nodes.size();

  graph = &graph_;
  start_ = clock::now();
  define_ip_model(graph_);

  cplex.setOut(env.getNullStream());
  // solve ip problem
  branch();
  end_ = clock::now();
  return best;
}
void McpIpSolver::define_ip_model(Graph &graph) {
  // var[i] = 1 <=> i-th node included in solution
  // F = Σ var[i] ⟶ max
  // var[i] + var[j] <= e[i, j] + 1

  unsigned long N = graph.nodes.size();
  for (int i = 0; i < N; ++i) {
    var.add(IloNumVar(env, 0, 1));
    var[i].setName((std::string("v") + std::to_string(i)).c_str());
  }

  // add constraints

  for (int j = 0; j < N; ++j) {
    for (int i = j + 1; i < N; ++i) {
      int e = graph.is_adjacent(i, j) ? 1 : 0;
      con.add(var[i] + var[j] <= 1 + e);
    }
  }

  // additional constraints
  

  size_t is_cons = 0, col_cons = 0;
  for (size_t k = 0; k < N / 2; ++k) {
    // use coloring as constraints
    auto coloring = get_coloring();
    upper_ = std::min(upper_, static_cast<const size_t &>(coloring.second));
    std::map<int, std::vector<size_t>> color_classes;

    for (size_t i = 0; i < N; ++i) {
      color_classes[coloring.first[i]].push_back(i);
    }

    for (auto &c: color_classes) {
      IloNumVarArray vars(env);
      for (auto &v: c.second) {
        vars.add(var[v]);
      }
      auto cons = IloSum(vars) <= 1;
      auto p = heu_bounds.emplace(cons);

      if (p.second && vars.getSize() > 2) {
        model.add(p.first->get());
        col_cons++;
      }
    }

    // use IS as constraints
    auto ind_set = get_independent_set();
    IloNumVarArray is_vars(env);
    for (auto &v: ind_set) {
      is_vars.add(var[v]);
    }
    auto ie_cons = IloSum(is_vars) <= 1;
    auto p = heu_bounds.emplace(ie_cons);

    if (p.second && is_vars.getSize() > 2) {
      model.add(p.first->get());
      is_cons++;
    }
  }

  std::cout << "Independent set constrints: " << is_cons << std::endl;
  std::cout << "Coloring constrints: " << col_cons << std::endl;

  obj = IloAdd(model, IloMaximize(env, IloSum(var), "K"));

  model.add(con);
  cplex = IloCplex(model);
}

MaxCliqueResult McpIpSolver::extract_clique() {
  MaxCliqueResult res{};

  IloNumArray vals(env);
  cplex.getValues(var, vals);

  for (size_t i = 0; i < vals.getSize(); ++i) {
    if (fabs(vals[i] - 1) < 1e-6) {
      res.nodes.push_back(i);
    }
  }

  return res;
}

void McpIpSolver::_print_solution() const {
  IloNumArray vals(env);
  env.out() << endl;
  env.out() << "Solution status = " << cplex.getStatus() << endl;
  env.out() << "Solution value  = " << cplex.getObjValue() << endl;
  cplex.getValues(vals, var);
  env.out() << "Values        = " << vals << endl;
}
void McpIpSolver::_print_model() {
  env.out() << "Objective: " << endl;
  env.out() << obj << endl;
  env.out() << "Constraints" << endl;
  env.out() << con << endl;
}

int McpIpSolver::branch(const Bound &bound) {
  static int it_no = 0;
  if (bound) { model.add(bound.get()); }
  cplex.solve();

  if (cplex.getStatus() == IloAlgorithm::Status::Infeasible) {
    return 0;
  }

  IloNumArray vals(env);
  cplex.getValues(var, vals);
  IloNum val = cplex.getObjValue();

  std::cout << "UB: " << upper_ 
            << " current LB: " << lower_ 
            << " branch LB: " << best.nodes.size() 
            << " current relaxation: " << val
            << " n: " << branch_vars.size() 
            << " i: " << (it_no++) << std::endl;
  // current branch ub
  int current = static_cast<int >(floor(val));

  // less than global lb
  // prune subtree
  if (current <= lower_ || current <= best.nodes.size()) {
    return 0;
  }

  // solution is integer
  bool all_int = true;
  for (size_t j = 0; j < vals.getSize(); ++j) {
    if (!is_integer(vals[j])) {
      all_int = false;
      break;
    }
  }

  if (all_int) {
    best = extract_clique();
    if (current == upper_) {
      best.status = MaxCliqueResult::EXACT;
    }
    return current;
  }

  // i - branching var
  int i = -1;
  double max_fraction = 0.0;
  for (int j = 0; j < vals.getSize(); ++j) {
    if (branch_vars.find(j) != branch_vars.end()) 
      continue;
    if (is_integer(vals[j]))
      continue;

    double fraction = vals[j] - floor(vals[j]);
    if (fraction > max_fraction) {
      i = j;
      max_fraction = fraction;
    }
  }

  if (i == -1) {
    return 0;
  }

  branch_vars.insert(i);
  IloRange branch1 = var[i] <= 0;
  IloRange branch2 = var[i] >= 1;
  if (try_branch(var[i] <= 0) < try_branch(var[i] >= 1)) {
    std::swap(branch1, branch2);
  }

  int branch_val = branch(branch1);

  // check if current branch ub is reached
  if (branch_val >= current || best.nodes.size() == upper_) {
    branch_vars.erase(i);
    return current;
  }
  branch_val = std::max(branch_val, branch(branch2));
  branch_vars.erase(i);

  // return best integer solution of current branch
  return branch_val;
}

double McpIpSolver::try_branch(IloRange range) {
  model.add(range);
  cplex.solve();

  if (cplex.getStatus() == IloAlgorithm::Status::Infeasible) {
    return 0;
  }

  IloNum d = cplex.getObjValue();
  range.end();

  return d;
}

pair<vector<int>, int> McpIpSolver::get_coloring() {
  // randomized coloring
  std::vector<int> colors(graph->nodes.size(), -1);

  // permute array
  std::vector<size_t> p(graph->nodes.size());
  std::iota(p.begin(), p.end(), 0);
  std::shuffle(p.begin(), p.end(), rd);

  int num_colors = 0;
  for (size_t & i : p) {
    std::unordered_set<int> used_colors;
    for (auto &neighbour: graph->nodes[i].adj) {
      used_colors.insert(colors[neighbour]);
    }

    bool colored = false;
    for (int c = 0; c < num_colors; ++c) {
      if (used_colors.find(c) == used_colors.end()) {
        colored = true;
        colors[i] = c;
        break;
      }
    }

    if (!colored) {
      colors[i] = num_colors++;
    }
  }

  return {colors, num_colors};
}

std::vector<int> McpIpSolver::get_independent_set() {
  std::vector<int> p(graph->nodes.size());
  std::iota(p.begin(), p.end(), 0);

  std::sort(p.begin(), p.end(), [&](int i, int j) {
    return graph->nodes[i].adj.size() < graph->nodes[j].adj.size();
  });

  // randomly select first element
  auto it = p.begin() + (rd() % p.size());
  std::rotate(p.begin(), it, it + 1);

  std::unordered_set<int> V(p.begin(), p.end());
  std::vector<int> res;
  for (auto &i: p) {
    if (V.find(i) == V.end()) {
      continue;
    }

    res.push_back(i);
    for (auto &neighbour: graph->nodes[i].adj) {
      V.erase(neighbour);
    }
  }

  return res;
}

bool Bound::operator==(const Bound & o) {
  std::unordered_map<int, double > m1, m2;

  if (r.getUb() != o.r.getUb() || r.getLb() != o.r.getLb()) {
    return false;
  }

  IloExpr::LinearIterator it;
  for (it = IloExpr(r.getExpr()).getLinearIterator(); it.ok(); ++it) {
    m1.emplace(it.getVar().getId(), it.getCoef());
  }
  for (it = IloExpr(o.r.getExpr()).getLinearIterator(); it.ok(); ++it) {
    m2.emplace(it.getVar().getId(), it.getCoef());
  }

  return m1 == m2;
}
