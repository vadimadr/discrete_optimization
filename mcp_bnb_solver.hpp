
#ifndef DISCRETE_OPTIMIZATION_MCPBRANCHANDBOUNDSOLVER_HPP
#define DISCRETE_OPTIMIZATION_MCPBRANCHANDBOUNDSOLVER_HPP

#include "imcp_solver.hpp"
class MCPBranchAndBoundSolver : public IMCPSolver {

    Graph *graph_;
public:

    MaxCliqueResult solve(Graph &graph) override;

    int max_core;
    std::vector<int> degree, k_core, k_core_perm;
    void reduce_graph(Graph &graph, std::vector<bool> &is_pruned, int &lb);
    void branch(std::vector<int> &p_vec,
                std::vector<bool> &ind,
                std::vector<int> &c_vec,
                std::vector<int> &result,
                std::vector<bool> &is_pruned,
                int &mc);
    bool not_reached_ub;
};

#endif //DISCRETE_OPTIMIZATION_MCPBRANCHANDBOUNDSOLVER_HPP
