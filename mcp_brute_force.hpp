
#ifndef DISCRETE_OPTIMIZATION_MCPBRUTEFORCE_HPP
#define DISCRETE_OPTIMIZATION_MCPBRUTEFORCE_HPP

#include <map>
#include "imcp_solver.hpp"

class MCPBruteForceSolver : public IMCPSolver {
    typedef std::vector<bool> subset_ind_t;
    typedef std::vector<int> subset_t;

    int64_t num_subgraphs(int64_t k, int64_t n);
    void kth_subset(int64_t k, int r, size_t n, subset_t &res, int l);
    bool next_subset(subset_t &p, size_t n);

    std::vector<int> mapping;
public:
    MaxCliqueResult solve(Graph &graph) override;
    size_t reduce_graph(Graph &graph, int64_t k);
    void initialize_mapping(Graph &graph);
    void remap_nodes(Graph::adj_type &result, std::vector<int> const &mapping);
};

#endif //DISCRETE_OPTIMIZATION_MCPBRUTEFORCE_HPP
