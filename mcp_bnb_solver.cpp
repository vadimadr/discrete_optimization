
#include <algorithm>
#include "mcp_bnb_solver.hpp"
#include "mcp_kcore_heu.hpp"

MaxCliqueResult MCPBranchAndBoundSolver::solve(Graph &graph) {
    MCPKCoreHeuristic k_core_heu;
    start_ = clock::now();

    const MaxCliqueResult &heuristic_res = k_core_heu.solve(graph);

    upper_ = k_core_heu.upper_;
    lower_ = heuristic_res.nodes.size();

    if (upper_ == lower_) {
        end_ = clock::now();
        return heuristic_res;
    }


    graph_ = &graph;

    MaxCliqueResult result{};

    degree = k_core_heu.degree;
    k_core = k_core_heu.k_core;
    k_core_perm = k_core_heu.k_core_perm;

    std::vector<bool> is_pruned(graph.nodes.size(), false);
    int mc_cur = lower_;

    reduce_graph(graph, is_pruned, mc_cur);

    std::vector<int> p_vec, C;
    const int max_size = k_core_heu.md + 1;
    p_vec.reserve(max_size);
    result.nodes.reserve(max_size);
    C.reserve(max_size);

    std::vector<bool> ind(graph.nodes.size(), false);

    std::vector<int> verts;
    for (size_t i = 0; i < graph.nodes.size(); ++i) {
        if (!is_pruned[i]) {
            verts.push_back(i);
        }
    }

    std::sort(verts.begin(), verts.end(), [&](int v1, int v2) {
        return k_core[v1] < k_core[v2];
    });

    int i = 0, u = 0;
#pragma omp parallel for schedule(dynamic) shared(is_pruned, graph, mc_cur, result) firstprivate(ind) private(u, p_vec, C)
    for (i = 0; i < (verts.size()) - (mc_cur - 1); ++i) {
        if (check_time()) {
            u = verts[i];
            if (k_core[u] > mc_cur) {
                p_vec.push_back(u);
                for (auto &edge: graph.nodes[u].adj) {
                    if (!is_pruned[edge] && k_core[edge] > mc_cur) {
                        p_vec.push_back(edge);
                    }
                }

                if (p_vec.size() > mc_cur) {
                    branch(p_vec, ind, C, result.nodes, is_pruned, mc_cur);
                }

                p_vec.clear();
            }

            is_pruned[u] = true;
        }
        else {
            result.status = MaxCliqueResult::TIME_LIMIT;
        }
    }

    end_ = clock::now();
    if (result.nodes.size() < lower_) {
        return heuristic_res;
    }
    return result;
}

void MCPBranchAndBoundSolver::reduce_graph(Graph &graph, std::vector<bool> &is_pruned, int &lb) {
    // TODO: graph reduction
    for (size_t i = 0; i < graph.nodes.size(); ++i) {
        int u = k_core_perm[i];
        if (k_core[u] <= lb || graph.nodes[u].adj.size() <= lb) {
            is_pruned[u] = true;
        }
    }
}

void MCPBranchAndBoundSolver::branch(std::vector<int> &p_vec,
                                     std::vector<bool> &ind,
                                     std::vector<int> &c_vec,
                                     std::vector<int> &result,
                                     std::vector<bool> &is_pruned,
                                     int &mc) {

    if (not_reached_ub) {
        while (p_vec.size() > 0) {

            if (c_vec.size() + p_vec.size() > mc) {
                int v = p_vec.back();
                c_vec.push_back(v);

                std::vector<int> intersect_;
                intersect_.reserve(p_vec.size());
                for (auto &item: graph_->nodes[v].adj)
                    ind[item] = true;

                for (int k = 0; k < p_vec.size() - 1; k++)
                    if (ind[p_vec[k]] && !is_pruned[p_vec[k]] && k_core[p_vec[k]] > mc)
                        intersect_.push_back(p_vec[k]);

                for (auto &item: graph_->nodes[v].adj)
                    ind[item] = false;

                if (intersect_.size() > 0) {
                    branch(intersect_, ind, c_vec, result, is_pruned, mc);
                }
                else if (c_vec.size() >= mc) {
#pragma omp critical
                    if (c_vec.size() > mc) {
                        mc = c_vec.size();
                        result = c_vec;
                        if (mc >= upper_) {
                            not_reached_ub = false;
                        }
                    }
                }
                    intersect_.clear();
                c_vec.pop_back();
            }
            else
                return;
            p_vec.pop_back();
        }
    }

}
