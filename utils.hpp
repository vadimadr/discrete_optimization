
#ifndef DISCRETE_OPTIMIZATION_UTILS_HPP
#define DISCRETE_OPTIMIZATION_UTILS_HPP

#include<string>
#include <iostream>

inline void massert(bool expr, const std::string &reason = "") {
    if (!expr) {
        if (!reason.empty()) {
            std::cout << "Error: " << reason;
        }
        else {
            std::cout << "Error occurred.";
        }
        exit(1);
    }
}

#endif //DISCRETE_OPTIMIZATION_UTILS_HPP
