#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "graph.hpp"
#include "utils.hpp"
#include "mcp_brute_force.hpp"
#include <iterator>
#include "mcp_kcore_heu.hpp"
#include "mcp_bnb_solver.hpp"
#include "mcp_ip_solver.h"

using namespace std::chrono_literals;


void print_clique(MaxCliqueResult const &clique) {
    std::cout << "Clique of size " << clique.nodes.size() << ":\n";
    const std::vector<int> &nodes = clique.nodes;
    std::copy(nodes.begin(), nodes.end(), std::ostream_iterator<int>(std::cout, " "));
}

int main(int argc, char* argv[]) {
//    std::vector<std::string> graphs = {"k3", "k5", "c5", "queen6_6", "myciel5"};
    std::vector<std::string> graphs = {std::string(argv[1])};
    massert(argc > 0, "Expected path to graph");

    for (auto &item: graphs) {
        std::string gr_path = item;
        std::cout << gr_path << std::endl;
        Graph gr = GraphReader::read_graph(gr_path);

        McpIpSolver solver;
        solver.set_time_limit(150s);


        const MaxCliqueResult &result = solver.solve(gr);

        std::cout << solver.get_secs() << "s" << std::endl;
        print_clique(result);
        std::cout << "\n\n";
    }

    return 0;
};
